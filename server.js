var express = require('express');
var path = require('path');
var reload = require('reload');
var GameController = require('./game.js');
var GameState = require('./game_state.js');

// Constants
const PORT = 5000;
const HOST = '0.0.0.0';
const app = express();

app.use(express.json());
app.use(express.static(path.join(__dirname, 'public')));

app.get("/", (req, res) => {
  console.log("got request");

  const filePath = path.resolve("public/index.html");
  res.sendFile(filePath);
});


async function start() {
  try {
    await reload(app);
  } catch (err) {
    console.error("Error with reload", err);
  }

  app.listen(PORT, HOST);
  console.log(`Example app listening at http://${HOST}:${PORT}`);
}

let intervalID;
let gameController = new GameController(intervalID, new GameState());

/**
 * Socket.io For Express
 */
var expressWs = require('express-ws')(app);

app.ws('/socket', (socket, req) => {
  console.log("new socket connection");

  socket.on("message", (message) => {

    console.log(`socketID: ${socket.id} message received = `, message);

    gameController.acceptMessage(socket, message);

    // Echo the message back to the client
    // socket.send(message);
  });

  socket.on("close", (code) => {
    console.log("socket closed", code);
    gameController.removeSocket(socket);
    gameController.dieSnakeByID(socket.id);
    gameController.broadCastGameState();
  });

  socket.on("error", (error) => console.log("socket error", error));
});

start();
