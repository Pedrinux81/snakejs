class Point {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    equals(point) {
        return point.x === this.x && point.y === this.y;
    }

    clone() {
        return new Point(this.x, this.y);
    }
}

class Apple {
    constructor(point) {
        this.x = point.x;
        this.y = point.y;
        this.num = 1;
    }
}

class Snake {

    constructor(segments = [], color, position, id, direction, gridSize, username) {
        this.direction = direction;
        this.color = color;
        this.id = id;
        this.segments = segments;
        this.score = 0;
        this.segments.push(position);
        this.grow(3);
        this.gridSize = gridSize;
        this.username = username;
        this.state = "live";
    }

    move() {
        // Copy each element to the previous one
        for (let i = this.segments.length - 1; i > 0; i--) {
            this.segments[i] = this.segments[i - 1].clone();
        }

        // Advance the head in the current direction
        if (this.direction === "left") {
            this.segments[0].x = (this.segments[0].x + this.gridSize.x - 1) % this.gridSize.x;
        } else if (this.direction === "right") {
            this.segments[0].x = (this.segments[0].x + this.gridSize.x + 1) % this.gridSize.x;
        } else if (this.direction === "up") {
            this.segments[0].y = (this.segments[0].y + this.gridSize.y - 1) % this.gridSize.y;
        } else if (this.direction === "down") {
            this.segments[0].y = (this.segments[0].y + this.gridSize.y + 1) % this.gridSize.y;
        } else {
            throw new Error("Invalid direction");
        }
    }

    changeDirection(direction) {
        this.direction = direction;
    }

    grow(amount) {
        // Add a bunch of dummy segments to the end
        const lastSegment = this.segments[this.segments.length - 1];
        for (let i = 0; i < amount; i++) {
            this.segments.push(lastSegment.clone());
        }
    }

    // Detect collision between head and body
    hitsObstacles(gameState) {
        let length = this.segments.length;
        let head = this.segments[0];

        // Detect collision with another segment of body
        for(let i = 1; i < length; i++) {
            if(head.equals(this.segments[i])) return true;
        }

        length = gameState.walls.length;
        for(let i = 0; i < length; i++) {
            if(head.equals(gameState.walls[i])) return true;
        }
        return false;
    }

    // Detection snake head touch a food
    hitsApple(gameState) {
        return this.segments[0].equals(gameState.apple);
    }

    updateDirection(direction) {
        if (!this.areOppositeDirections(this.direction, direction)) {
            this.direction = direction;
        }
    }

    upScore(num) {
        this.score += num;
    }

    returnToBabySnake(position) {
        // Move snake head to new postion
        this.segments[0] = position.clone();
        // Delete all segments except head segment of snake
        let length = this.segments.length-1;
        this.segments.splice(1, length-1);
        // grow snake
        this.grow(3);
    }

    areOppositeDirections(a, b) {
        return (
            (a === "left" && b === "right") ||
            (a === "right" && b === "left") ||
            (a === "up" && b === "down") ||
            (a === "down" && b === "up")
        );
    }

}

class GameState {

    constructor() {
        this.gridSize = new Point(64, 36);
        this.status = "ready";
        this.snakes = [];
        this.death_snakes = [];
        this.walls = [];
        this.apple = new Apple(new Point());
        this.level = 1;
        this.createWalls();
        this.moveApple(1).then();
    }

    play() {
        this.status = "playing";
    }

    update() {
        //
    }

    restart() {
        this.status = "ready";
        this.moveApple(1).then();
        this.level = 1;
        this.snakes = [];
        this.death_snakes = [];
        this.walls = [];

        this.createWalls();
    }

    addSnake(id, color, username) {
        this.snakes.push(new Snake([], color, this.findEmptyPosition(1,this.gridSize.x/3,1,this.gridSize.y-2),
            id, "right", this.gridSize, username));
    }

    findSnakeByUID(id) {
        for(let snake of this.snakes) {
            if(snake.id === id) return snake;
        }
        return null;
    }

    createWalls() {

        // Top and bottom walls
        for(let x = 0; x < this.gridSize.x; x++) {
            this.walls.push(new Point(x,0));
            this.walls.push(new Point(x, this.gridSize.y-1));
        }

        // Left and right walls
        for(let y = 1; y < this.gridSize.y; y++) {
            this.walls.push(new Point(0, y));
            this.walls.push(new Point(this.gridSize.x-1, y));
        }

    }

    async moveApple(num = 1) {

        let newPosition = this.findEmptyPosition(1, this.gridSize.x-2, 1, this.gridSize.y-2);

        this.apple.x = newPosition.x;
        this.apple.y = newPosition.y;
        this.apple.num = num;
    }

    upLevel() {
        this.level++;
        this.addObstaclesToScreen(2).then();
    }

    findEmptyPosition(minX = 1, maxX = 10, minY = 1, maxY = 10) {
        let x = Math.floor(Math.random() * (maxX)) + minX;
        let y = Math.floor(Math.random() * (maxY)) + minY;

        // Not in walls positions
        for(let wall of this.walls) {
            if(wall.x === x && wall.y === y) {
                this.findEmptyPosition(minX, maxX, minY, maxY);
            }
        }

        // Not in others snakes positions
        for(let snake of this.snakes) {

            for(let j = 0; j < snake.segments.length; j++) {
                if(snake.segments[j].x === x && snake.segments[j].y === y) {
                    this.findEmptyPosition(minX, maxX, minY, maxY);
                }
            }
        }

        return new Point(x,y);
    }

    async addObstaclesToScreen(num) {
        for(let i = 0; i < num; i++) {
            this.walls.push(this.findEmptyPosition(5, this.gridSize.x-5, 5, this.gridSize.y - 5));
        }
    }

}

/////////////////////////////////////////////////////////////////////////////////////////////////
//                                      Main Document                                          //
/////////////////////////////////////////////////////////////////////////////////////////////////


let keyDown = null;
const cellSize = 15;

let onePlayerMode = null;

const direction = {
    UP: "up",
    RIGHT: "right",
    DOWN: "down",
    LEFT: "left"
};

let gameState = null;
let mySnake = null;

let timerSendDirection = null;
let newDirection = null;



function initButtons() {
    // Initial state buttons
    document.getElementById("sendButton").hidden = false;
    document.getElementById("disconnect_button").hidden = true;
    document.getElementById("pause_button").hidden = true;
    document.getElementById("play_button").hidden = true;
    document.getElementById("info_connections").hidden = true;
    document.getElementById("level").hidden = true;
    document.getElementById("player_list").hidden = true;
    document.getElementById("exit_button").hidden = true;
}

initButtons();

class User {
    constructor(socket, nickname, id, color) {
        this.socket = socket;
        this.nickname = nickname;
        this.id = id;
        this.color = color;
    }
}

let user = null;
let gameMode = null;
let snake = null;

// Select option game mode
let optionSelectGameMode = document.getElementById("gameMode");
optionSelectGameMode.addEventListener('change', (target)=> {

    clear();
    initButtons();

    if (optionSelectGameMode.value === "local") {
        if(user !== null) { user.socket.close(); }

        document.getElementById("sendButton").value = "New Game";
    } else {
        document.getElementById("sendButton").value = "Connection";
    }

});

function clear() {
    const canvas = document.getElementsByTagName("canvas")[0];
    const ctx = canvas.getContext("2d");
    // Clear screen
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}

function gameScreenRefresh() {
    const canvas = document.getElementsByTagName("canvas")[0];
    const ctx = canvas.getContext("2d");

    // document.getElementById("frames").innerHTML = `Frames Count: ${frameCount}`;
    document.getElementById("level").innerHTML = `Level: ${gameState.level.toString()}`;
    displayListInfoSnakes();

    // Clear screen
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    drawWalls(ctx);
    drawApple(ctx);
    drawSnakes(ctx);
}

function drawSnakes(ctx) {
    // draw all snakes from gameState
    for(const snake of gameState.snakes) {

        ctx.fillStyle = snake.color;
        for(const seg of snake.segments) {
            ctx.fillRect(
                seg.x * cellSize,
                seg.y * cellSize,
                cellSize,
                cellSize);
        }
    }
}

function addKeyboardEventHandler() {
    // const canvas = document.getElementsByTagName("canvas")[0];
    window.addEventListener("keydown", onKeyDown);
}


function drawWalls(ctx) {

    // Draw All Walls
    ctx.fillStyle = "deeppink";
    for(const wall of gameState.walls) {
        ctx.fillRect(wall.x * cellSize, wall.y * cellSize, cellSize, cellSize);
    }
}


function onKeyDown(event) {

    event.preventDefault();

    keyDown = event.key;
    switch (keyDown) {
        case "ArrowUp":
            // if (mySnake.direction !== direction.DOWN) {
            //     mySnake.direction = direction.UP;
            //     // Send new change direction to server socket
            //     user.socket.send(JSON
            //         .stringify({"type": "changeDirection", "direction": mySnake.direction}))
            // }

            newDirection = direction.UP;
            // user.socket.send(JSON.stringify({"type": "changeDirection", "direction": direction.UP}));
            break;
        case "ArrowRight":
            // if (mySnake.direction !== direction.LEFT) {
            //     mySnake.direction = direction.RIGHT;
            //     // Send new change direction to server socket
            //     user.socket.send(JSON
            //         .stringify({"type": "changeDirection", "direction": mySnake.direction}))
            // }
            newDirection = direction.RIGHT;
            // user.socket.send(JSON.stringify({"type": "changeDirection", "direction": direction.RIGHT}));
            break;
        case "ArrowDown":
            // if (mySnake.direction !== direction.UP) {
            //     mySnake.direction = direction.DOWN;
            //     // Send new change direction to server socket
            //     user.socket.send(JSON
            //         .stringify({"type": "changeDirection", "direction": mySnake.direction}))
            // }
            newDirection = direction.DOWN;
            // user.socket.send(JSON.stringify({"type": "changeDirection", "direction": direction.DOWN}));
            break;
        case "ArrowLeft":
            // if (mySnake.direction !== direction.RIGHT) {
            //     mySnake.direction = direction.LEFT;
            //     // Send new change direction to server socket
            //     user.socket.send(JSON
            //         .stringify({"type": "changeDirection", "direction": mySnake.direction}))
            // }
            newDirection = direction.LEFT;
            // user.socket.send(JSON.stringify({"type": "changeDirection", "direction": direction.LEFT}));
            break;
    }

}

function play() {
    if(gameMode === "online") {
        // Send status play to server socket
        user.socket.send(JSON
            .stringify({"type": "play"}));
    } else {
        onePlayerMode.play();
        document.getElementById("play_button").hidden = true;
        document.getElementById("pause_button").hidden = false;
    }
}

function pause() {
    onePlayerMode.pause();
    document.getElementById("play_button").hidden = false;
    document.getElementById("pause_button").hidden = true;
}

function exit() {
    if(onePlayerMode !== null) {
        onePlayerMode.stopGame();
        onePlayerMode = null;
    }
    clear();
    initButtons();

}

function drawApple(ctx) {
    // Draw All Walls
    ctx.fillStyle = "white";
    ctx.font = '20px arial black';
    // y: (gameState.apple.y+1) because text write in bottom to up
    ctx.fillText(gameState.apple.num.toString(), gameState.apple.x * cellSize, (gameState.apple.y+1) * cellSize);
}


function displayListInfoSnakes() {

    gameState.snakes.sort( function(a , b){
        if(a.score < b.score) return 1;
        if(a.score > b.score) return -1;
        return 0;
    });


    let tablePlayers = document.getElementById("table_players");
    tablePlayers.innerText = "";

    let length = gameState.snakes.length;

    for(let i = 0; i < length; i++) {
        let snake = gameState.snakes[i];
        let username = snake.username;
        if (snake.id === user.id) {
            mySnake = snake;
            username += "*";
        }

        tablePlayers.innerHTML += "<tr>\n" +
            "<td style=\"width: 50%;\">"+ username + "</td>\n" +
            "<td style=\"width: 25%;\">"+ snake.score + "</td>\n" +
            "<td style=\"width: 25%; background-color: "+ snake.color +";\"</td>\n" +
            "</tr>";

    }
}


// http://guid.us/GUID/JavaScript
function createGuid(){
    /**
     * @return {string}
     */
    function S4() {
        return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
    }
    return (S4() + S4() + "-" + S4() + "-4" + S4().substr(0,3) + "-" + S4() + "-" + S4() + S4() + S4()).toLowerCase();
}

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function getRandColor() {
    const months = ["khaki", "yellow", "pink", "purple", "cyan", "green", "grey", "goldenrod", "magenta",
        "orchid", "orange", "red", "salmon", "violet", "turquoise", "sienna", "black", "white", "aqua", "brown"];

    return months[Math.floor(Math.random() * months.length)];
}

function submitNickname() {
    let nickname = document.getElementById("nickname").value;
    let mode = document.getElementById("gameMode").value;
    gameMode = mode;

    if (user === null) {
        let socket = null;
        user = new User(socket, nickname, createGuid(), getRandColor());
    } else {
        user.color = getRandColor();
    }

    if (mode === "online") {
        user.socket = connectWebSocket(user);
    } else {
        if(user.socket !== null) {
            user.socket.close();
        }
        onePlayerMode = new OnePlayerMode(nickname);
        document.getElementById("level").hidden = false;
        // document.getElementById("level").innerText = `Level ${onePlayerMode.gameState.level} ~ ${user.nickname} ~ Score: ${onePlayerMode.gameState.snakes[0].score}`;
        onePlayerMode.initializeGame();
        document.getElementById("sendButton").hidden = true;
        document.getElementById("exit_button").hidden = false;
        document.getElementById("play_button").hidden = false;

    }
}


class OnePlayerMode {

    UP_SCORE_WHEN_EAT_FOOD = 100;
    UP_SCORE_EVERY_LIVE_LOOP = 1;

    direction = {
        UP: 0,
        RIGHT: 1,
        DOWN: 2,
        LEFT: 3
    };

    keyDown = null;
    cellSize = 15;

    constructor(username) {
        this.timeout = 400;
        this.id = "1";
        this.username = username;
        this.gameState = new GameState();
        this.color = "orange";
        this.gameState.addSnake(this.id, this.color, username);
        this.initializeGame();
    }

    displayInfo() {

        document.getElementById("level").innerText = `Level ${this.gameState.level} ~ ${this.username} ~ Score: ${this.gameState.snakes[0].score}`;
    }

    gameLoop() {

        const canvas = document.getElementsByTagName("canvas")[0];
        const ctx = canvas.getContext("2d");

        // document.getElementById("frames").innerHTML = `Frames Count: ${frameCount}`;
        document.getElementById("level").innerHTML = `Level: ${this.gameState.level.toString()}`;

        // Clear screen
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        this.displayInfo();

        this.drawWalls(ctx);
        this.drawApple(ctx);

        let snake = this.gameState.snakes[0];

        snake.move();
        this.drawSnake(ctx);

        // Detect collisions
        if (snake.hitsObstacles(this.gameState)) {
            console.log("Collision!!!!");
            this.gameState.state = "over";
        }

        // Check snake eat food
        if(snake.hitsApple(this.gameState)) {
            if (this.gameState.apple.num < 9) {
                snake.grow(3);
                this.gameState.apple.num++;
                this.gameState.moveApple(this.gameState.apple.num).then();
            } else {
                this.gameState.upLevel();
                this.gameState.moveApple().then();
                snake.returnToBabySnake(snake.segments[0]);
            }
            snake.upScore(this.UP_SCORE_WHEN_EAT_FOOD);
        }

        snake.upScore(this.UP_SCORE_EVERY_LIVE_LOOP);

        if (this.gameState.state === 'over') {
            this.stopGame();
        }

    }

    initializeGame() {

        const canvas = document.getElementsByTagName("canvas")[0];
        const ctx = canvas.getContext("2d");

        canvas.addEventListener("keydown", this.onKeyDown);
        this.gameState.gridSize.width = canvas.width / this.cellSize;
        this.gameState.gridSize.height = canvas.height / this.cellSize;

        this.displayInfo();

        this.drawWalls(ctx);
        this.drawApple(ctx);
        this.drawSnake(ctx);


    }

    startGame() {
        if (this.gameState.status === "over") {
            this.timeout = 400;
            this.gameState.restart();
            this.gameState.addSnake(this.id, this.color, this.username);
        }
        // Set status to playing mode and start timer for game loop
        this.gameState.play();
        this.intervalid = setInterval(() => this.gameLoop(), this.timeout);
        // window.requestAnimationFrame(this.gameLoop);
    }

    stopGame() {

        this.gameState.status = "over";
        clearInterval(this.intervalid);
        // And restart the game state for new sockets connections
        this.gameState.restart();
    }

    pause() {
        this.gameState.state = "pause";
        clearInterval(this.intervalid);
    }

    play() {
        this.startGame();
    }

    drawWalls(ctx) {

        let length = this.gameState.walls.length;
        // Draw All Walls
        ctx.fillStyle = "deeppink";
        for(let i = 0; i < length; i++) {
            ctx.fillRect(this.gameState.walls[i].x * this.cellSize, this.gameState.walls[i].y * this.cellSize,
                this.cellSize, this.cellSize);
        }
    }

    drawApple(ctx) {
        ctx.fillStyle = "white";
        ctx.font = '20px arial black';
        // y: (gameState.apple.y+1) because text write in bottom to up
        ctx.fillText(this.gameState.apple.num.toString(), this.gameState.apple.x * this.cellSize,
            (this.gameState.apple.y+1) * this.cellSize);
    }

    drawSnake(ctx) {
        let snake = this.gameState.snakes[0];

        // Head
        let head = snake.segments[0];
        ctx.fillStyle = snake.color;
        ctx.fillRect(
            head.x * this.cellSize,
            head.y * this.cellSize,
            this.cellSize,
            this.cellSize);

        // Tail
        ctx.fillStyle = `dark${snake.color.toUpperCase()}`;
        for(let i = 1; i < snake.segments.length; i++) {
            ctx.fillRect(
                snake.segments[i].x * this.cellSize,
                snake.segments[i].y * this.cellSize,
                this.cellSize,
                this.cellSize);
        }
    }

    onKeyDown(event) {
        event.preventDefault();

        let snake = this.gameState.snakes[0];

        console.log(this.gameState.snakes.length);
        console.log(snake);

        this.keyDown = event.key;
        console.log(keyDown);

        switch (this.keyDown) {
            case "ArrowUp":
                if (snake.dir !== this.direction.DOWN) {
                    snake.dir = this.direction.UP;
                }

                break;
            case "ArrowRight":
                if (snake.dir !== this.direction.LEFT) {
                    snake.dir = this.direction.RIGHT;
                }
                break;
            case "ArrowDown":
                if (snake.dir !== this.direction.UP) {
                    snake.dir = this.direction.DOWN;
                }

                break;
            case "ArrowLeft":
                if (snake.dir !== this.direction.RIGHT) {
                    snake.dir = this.direction.LEFT;
                }
                break;
        }
    }

}


//////////////////////////
//    Connection        //
//////////////////////////


function connectWebSocket(user) {
    const url = `ws://${window.location.host}/socket`;
    let socket = new WebSocket(url);

    socket.onopen = () => {
        console.log("socket connected!");

        // Save socket guid and send user connection to server
        socket.id = user.id;
        user.socket = socket;
        socket.send(JSON.stringify({
            "type": "connection",
            "user": {
                "id": user.id,
                "color": user.color,
                "username": user.nickname
            }
        }));

        // Set select options to check list format palette
        document.getElementById("player_list").hidden = false;
        document.getElementById("title_infos").innerHTML = "<b>Ranking Snakes</b> <span style='font-size: 14px; color: gray'>* You</span>";
        document.getElementById("disconnect_button").hidden = false;
        document.getElementById("sendButton").hidden = true;
        document.getElementById("info_connections").hidden = false;

        addKeyboardEventHandler();
        timerSendDirection = setInterval(() => sendChangeDirection(), 400);
    };

    socket.onmessage = (message) => {
        console.log("got message from socket", message);
        parseMessage(message);
    };

    socket.onclose = () => {
        // document.getElementById("player_list").hidden = true;
        document.getElementById("disconnect_button").hidden = true;
        document.getElementById("play_button").hidden = true;
        document.getElementById("sendButton").hidden = false;
        document.getElementById("info_connections").hidden = true;
        document.getElementById("info_connections").innerText = "Waiting players...";
        gameState = null;
        mySnake = null;
        newDirection = null;
        clearInterval(timerSendDirection);
    };

    socket.onerror = (error) => console.error("socket error", error);

    return socket;
}

function parseMessage(message) {
    gameState = JSON.parse(message.data).gameState;
    window.requestAnimationFrame(gameScreenRefresh);
    // Over
    if(gameState.status === "over") {
        if(gameState.snakes.length === 1) {
            let winner = gameState.snakes[0];
            if (winner.id === user.id) {
                alert(`~~ ¡¡¡ YOU WIN !!! ~~`);
            } else {
                alert(`The Winner Is ~~ ${winner.username} ~~`);
            }
        } else {
            alert(`.-*@ GAME OVER @*-.`);
        }
        user.socket.close();
        clearInterval(this.timerSendDirection);

    // Playing
    } else if(gameState.status === "playing") {
        document.getElementById("info_connections").hidden = true;

    } else if(gameState.status === "ready") {
        if(gameState.snakes.length >= 2 && gameState.snakes[0].id === user.id) {
            document.getElementById("play_button").hidden = false;
            document.getElementById("info_connections").innerText = "Your are the MASTER!!. You can start game or wait more players...";
        } else {
            document.getElementById("play_button").false = true;
            document.getElementById("info_connections").hidden = false;
        }
    }

}


function sendChangeDirection() {
    if (user === null || newDirection === null) return;
    user.socket.send(JSON.stringify({"type": "changeDirection", "direction": newDirection}));
    newDirection = null;
    keyDown = null;
}


