var Point = require('./point.js');
var Snake = require('./snake.js');
var Apple = require('./apple.js');

module.exports = class GameState {

    constructor() {
        this.gridSize = new Point(64, 36);
        this.status = "ready";
        this.snakes = [];
        this.death_snakes = [];
        this.walls = [];
        this.apple = new Apple(new Point());
        this.level = 1;
        this.moveApple(1).then(()=> this.createWalls());
    }

    play() {
        this.status = "playing";
    }

    update() {
        //
    }

    restart() {
        this.status = "ready";
        this.moveApple(1).then();
        this.level = 1;
        this.snakes = [];
        this.death_snakes = [];
        this.walls = [];

        this.createWalls();
    }

    addSnake(id, color, username) {
        this.snakes.push(new Snake([], color, this.findEmptyPosition(1,this.gridSize.x/3,1,this.gridSize.y-2),
            id, "right", this.gridSize, username));
    }

    findSnakeByUID(id) {
        for(let snake of this.snakes) {
            if(snake.id === id) return snake;
        }
        return null;
    }

    createWalls() {

        // Top and bottom walls
        for(let x = 0; x < this.gridSize.x; x++) {
            this.walls.push(new Point(x,0));
            this.walls.push(new Point(x, this.gridSize.y-1));
        }
    
        // Left and right walls
        for(let y = 1; y < this.gridSize.y; y++) {
            this.walls.push(new Point(0, y));
            this.walls.push(new Point(this.gridSize.x-1, y));
        }
    
    }

    async moveApple(num = 1) {

        let newPosition = this.findEmptyPosition(1, this.gridSize.x-2, 1, this.gridSize.y-2);

        this.apple.x = newPosition.x;
        this.apple.y = newPosition.y;
        this.apple.num = num;
    }

    upLevel() {
        this.level++;
        this.addObstaclesToScreen(2).then();
    }

    findEmptyPosition(minX = 1, maxX = 10, minY = 1, maxY = 10) {
        let x = Math.floor(Math.random() * (maxX)) + minX;
        let y = Math.floor(Math.random() * (maxY)) + minY;

        // Not in walls positions
        for(let wall of this.walls) {
            if(wall.x === x && wall.y === y) {
                this.findEmptyPosition(minX, maxX, minY, maxY);
            }
        }

        // Not in others snakes positions
        for(let snake of this.snakes) {

            for(let j = 0; j < snake.segments.length; j++) {
                if(snake.segments[j].x === x && snake.segments[j].y === y) {
                    this.findEmptyPosition(minX, maxX, minY, maxY);
                }
            }
        }

        return new Point(x,y);
    }

    async addObstaclesToScreen(num) {
        for(let i = 0; i < num; i++) {
            this.walls.push(this.findEmptyPosition(5, this.gridSize.x-5, 5, this.gridSize.y - 5));
        }
    }

};