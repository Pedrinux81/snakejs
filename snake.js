
module.exports = class Snake {

    constructor(segments = [], color, position, id, direction, gridSize, username) {
        this.direction = direction;
        this.color = color;
        this.id = id;
        this.segments = segments;
        this.score = 0;
        this.segments.push(position);
        this.grow(3);
        this.gridSize = gridSize;
        this.username = username;
        this.state = "live";
    }

    move() {
        // Copy each element to the previous one
        for (let i = this.segments.length - 1; i > 0; i--) {
            this.segments[i] = this.segments[i - 1].clone();
        }

        // Advance the head in the current direction
        if (this.direction === "left") {
            this.segments[0].x = (this.segments[0].x + this.gridSize.x - 1) % this.gridSize.x;
        } else if (this.direction === "right") {
            this.segments[0].x = (this.segments[0].x + this.gridSize.x + 1) % this.gridSize.x;
        } else if (this.direction === "up") {
            this.segments[0].y = (this.segments[0].y + this.gridSize.y - 1) % this.gridSize.y;
        } else if (this.direction === "down") {
            this.segments[0].y = (this.segments[0].y + this.gridSize.y + 1) % this.gridSize.y;
        } else {
            throw new Error("Invalid direction");
        }
    }

    changeDirection(direction) {
        this.direction = direction;
    }

    grow(amount) {
        // Add a bunch of dummy segments to the end
        const lastSegment = this.segments[this.segments.length - 1];
        for (let i = 0; i < amount; i++) {
            this.segments.push(lastSegment.clone());
        }
    }

    // Detect collision between head and body
    hitsObstacles(gameState) {
        let length = this.segments.length;
        let head = this.segments[0];

        // Detect collision with another segment of body
        for(let i = 1; i < length; i++) {
            if(head.equals(this.segments[i])) return true;
        }

        length = gameState.walls.length;
        for(let i = 0; i < length; i++) {
            if(head.equals(gameState.walls[i])) return true;
        }
        return false;
    }

    // Detection snake head touch a food
    hitsApple(gameState) {
        return this.segments[0].equals(gameState.apple);
    }

    updateDirection(direction) {
        if (!this.areOppositeDirections(this.direction, direction)) {
            this.direction = direction;
        }
    }

    upScore(num) {
        this.score += num;
    }

    returnToBabySnake(position) {
        // Move snake head to new postion
        this.segments[0] = position.clone();
        // Delete all segments except head segment of snake
        let length = this.segments.length-1;
        this.segments.splice(1, length-1);
        // grow snake
        this.grow(3);
    }

    areOppositeDirections(a, b) {
        return (
            (a === "left" && b === "right") ||
            (a === "right" && b === "left") ||
            (a === "up" && b === "down") ||
            (a === "down" && b === "up")
        );
    }

};