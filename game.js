var Point = require('./point.js');

const cellSize = 15;
const snakeGrowth = 4;

const gridSize = new Point(960 / cellSize, 540 / cellSize);

/**
 * Class GameController for manage game, sockets, and others for run the game
 */
module.exports = class GameController {

    BODY_INCREMENT = 3;
    UP_SCORE_WHEN_EAT_FOOD = 100;
    UP_SCORE_EVERY_LIVE_LOOP = 10;

    constructor(intervalid, gameState) {
        this.intervalid = intervalid;
        this.gameState = gameState;
        this.socketTold = [];
        this.timeout = 400;
    }

    addSocket(socket) {
        // Add socket to list
        this.socketTold.push(socket);
        console.log("Socket added to list with id : " + socket.id);
    }

    removeSocket(socket) {
        for(let i = 0; i < this.socketTold.length; i++) {
            if(this.socketTold[i] === socket) {
                this.socketTold.splice(i, 1);
                return;
            }
        }
    }

    acceptMessage(socket, message) {
        // Parse message
        message = JSON.parse(message);
        // “getState” || “play” || “changeDirection”, direction: “left” || “restart”
        if (message.type === "changeDirection") {
            let snake = this.gameState.findSnakeByUID(socket.id);
            if (snake) {
                snake.updateDirection(message.direction);
            }

        } else if(message.type === "getState") {
            if(socket.readyState === 3) {
                console.log("SOCKET DISCONNECTED!!!");
            }
        } else if(message.type === "play") {

            if (this.gameState.status === "ready") {
                this.startGame();
            }
        } else if(message.type === "connection") {
            socket.id = message.user.id;
            this.addSocket(socket);
            this.gameState.addSnake(message.user.id, message.user.color, message.user.username);
            this.broadCastGameState();
        }
    }

    loop() {
        for(let snakeA of this.gameState.snakes) {

            let isSnakeADied = false;
            // Move snake
            snakeA.move();

            // Check status game to stop loop (security)
            if (this.gameState.status === "over") {
                return;
            }

            // Detect collisions with obstacles
            if (snakeA.hitsObstacles(this.gameState)) {
                console.log("Collision with wall!!");
                this.dieSnakeByID(snakeA.id);
                isSnakeADied = true;
                continue;
            }
            // Detect collisions with other snakes
            for(let snakeB of this.gameState.snakes) {

                if(snakeA.id === snakeB.id) {
                    continue;
                }

                // Check status game to stop loop (security)
                if (this.gameState.status === "over") {
                    return;
                }

                // Collision between two snake heads
                if(snakeB.segments[0].equals(snakeA.segments[0])) {
                    console.log("Collision between snake heads!!!");

                    // Win great snake!! ;-) Remove snake with minor score
                    if (snakeB.score > snakeA.score) {
                        this.dieSnakeByID(snakeA.id);
                        isSnakeADied = true;
                    // Death two snake when scores are equals
                    } else if (snakeB.score === snakeA.score) {
                        this.dieSnakeByID(snakeB.id);
                        this.dieSnakeByID(snakeA.id);
                        isSnakeADied = true;
                    } else {
                        this.dieSnakeByID(snakeB.id);
                    }
                    break;
                }

                if (isSnakeADied) { continue; }

                // Detect Collisions with another snake body segment
                for(let h = 1; h < snakeB.segments.length; h++) {
                    if(snakeB.segments[h].equals(snakeA.segments[0])) {
                        console.log("Collision between snakes!!");
                        // Update Score enemy
                        snakeB.upScore(200);
                        // Death snake made collision
                        this.dieSnakeByID(snakeA.id);
                        isSnakeADied = true;
                        break;
                    }
                }

            }

            if (isSnakeADied) { continue; }

            // Check snake ate food
            if(snakeA.hitsApple(this.gameState)) {
                console.log("Ate Apple!!!");
                snakeA.grow(this.BODY_INCREMENT);
                snakeA.upScore(this.UP_SCORE_WHEN_EAT_FOOD);
                this.gameState.apple.num++;
                this.gameState.moveApple(this.gameState.apple.num).then(r => {
                    if (this.gameState.apple.num % 5 === 0) {
                        this.gameState.upLevel();
                    }
                });


                // Augmented speed game
                clearInterval(this.intervalid);
                this.timeout -= 2;
                this.intervalid = setInterval(() => this.loop(), this.timeout);
            }
            // Up snake score when is not death
            snakeA.upScore(this.UP_SCORE_EVERY_LIVE_LOOP);

        }

        this.broadCastGameState();

    }

    broadCastGameState() {
        // Send gameState to everybody (sockets)
        for(let s of this.socketTold) {
            s.send(JSON.stringify({ "gameState": this.gameState }))
        }
    }

    startGame() {
        // Set status to playing mode and start timer for game loop
        this.gameState.play();
        this.intervalid = setInterval(() => this.loop(), 400);
    }

    stopGame() {

        this.gameState.status = "over";
        clearInterval(this.intervalid);
        // Publish end state game to sockets
        this.broadCastGameState();
        // And restart the game state for new sockets connections
        this.gameState.restart();
        // Return timeout to default value
        this.timeout = 400;

    }

    dieSnakeByID(id) {
        for(let i = 0; i < this.gameState.snakes.length; i++) {
            if(this.gameState.snakes[i].id === id) {
                console.log("Death Snake: ", this.gameState.snakes[i].id);
                // Add snake to death list for make ranking after
                this.gameState.death_snakes.push(this.gameState.snakes[i]);
                // And death bad snake!! (remove snake)
                this.gameState.snakes.splice(i,1);
                break;
            }
        }

        // Stop game when are snakes in the game...
        if(this.gameState.status === "playing" && this.gameState.snakes.length < 2 ) {
            this.stopGame();
        }
    }
};