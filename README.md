# SnakeJS

This is a student game project in JavaScript for emulate the mythic game Blockade in 1976 by Gremlin Industries. So,
famed as snake games by example Nibbles, written in QBasic by Rick Raddatz in 1991 for MS-DOS (Microsoft) operating 
systems.

This exercise implements the multi-gamers option with WebSockets technology and it's possible to control the game with
keyboard for manage and driving the Snake.

- Author:`Pedro CASTILLEJO`
- Date:`30 Nov 2020`
- Version:`0.1.0 beta`


### This is the link to play online 

vps694713.ovh.net:5000


### npm

For init a package manager write in console or terminal command line next line:
- `$ npm init`

Install libraries in our module with save option:
- `$ npm install NAME_LIBRARY --save`

Install Express and add "type": "module" in package.json:
- `$ npm install express --save`
- `"type": "module"`

Run app
- `$ nmp start`

Install nodemon for restarting automatic when detect changes in code:
- `$ npm i nodemon --save`

Next, add this line in package.json: 
- `"start": "npx nodemon FILE_TO_RUN` in "scripts" key value, for example:


    "scripts": {
        "start": "npx nodemon app.js -e js,html,css"
      }
The options "-e js,html,css" is for detect html, js and css files changes.

## Docker

For build and run the docker container and deploy the server games tap next lines in systems with Docker installed:

    $ ./build_docker.sh
    $ ./start_docker.sh

The server turn in port 5000 by default. 


# The Game

The have two game mode: One Player and Multi-Players Online. In online mode the first player connected is the
master of web socket room and has the possibility to start the game when exists two player or more.

The color of players is a random picked, the user must input the nickname to send connection and play.

## Game Strategy

Snakes eat numbers. Snake grow every time that ate one number. On multi-player mode when two snake collided, the snake
has made collision disparate of game.  When two snakes collided head-to-head the biggest snake is the winner, the less
snake disparate.  The game finish when rest only snake in screen!! 
  
 